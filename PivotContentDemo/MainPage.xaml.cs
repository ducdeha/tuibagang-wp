﻿namespace PivotContentDemo
{
    using System;
    using System.Windows.Controls;

    using Microsoft.Phone.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using PivotContentDemo.SubScreen;
    using System.Windows;
    using Logins;
    using Newtonsoft.Json;
    using System.Globalization;
    using Microsoft.Phone.Notification;
    using System.Text;
    using System.Diagnostics;
    using System.Collections.Generic;
    using Request;
    using Microsoft.Phone.Info;
    using Microsoft.Phone.Net.NetworkInformation;
    using System.Reflection;
    using Microsoft.Phone.Shell;
    using Windows.ApplicationModel.Activation;
    using Windows.Foundation;
    using Windows.UI.Notifications;
    using System.IO;
    using Newtonsoft.Json.Linq;
    using SubScreen.SubCaNhan;
    public partial class MainPage : PhoneApplicationPage
    {
        private DoiThuong mDoiThuong;
        private string mAppStatus ="0";
        

        public MainPage()
        {
            this.InitializeComponent();
            getAppStatus();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("item"))
            {
                try
                {
                    var index = NavigationContext.QueryString["item"];
                    var indexParsed = int.Parse(index);
                    if (indexParsed == 1)
                    {
                        Pivot.SelectedIndex = 3;
                    }
                    else if (indexParsed == 2)
                    {
                        Pivot.SelectedIndex = 1;
                    }
                    //Pivot.SelectedIndex = indexParsed;
                }
                catch (Exception eee)
                {
                }
            }
        }

        private void OnLoadingPivotItem(object sender, PivotItemEventArgs e)
        {
            int selectedIndex = ((Pivot)sender).SelectedIndex;
            showActionbar(selectedIndex != 4);
            if (selectedIndex == 0)
                selectedTab1();
            else if (selectedIndex == 1)
                selectedTab2();
            else if (selectedIndex == 2)
                selectedTab3();
            else if (selectedIndex == 3)
                selectedTab4();
            else if (selectedIndex == 4)
                selectedTab5();
            else
                selectedTab1();
            if (e.Item.Content != null)
            {
                if (selectedIndex == 2 && mDoiThuong != null) mDoiThuong.loadData(false);
                return;
            }
            var pivotItemContentControl = CreateUserControlForPivotItem(((Pivot)sender).SelectedIndex);
            e.Item.Content = pivotItemContentControl;
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Terminate();
            base.OnBackKeyPress(e);
        }

        private UserControl CreateUserControlForPivotItem(int selectedIndex)
        {
            switch (selectedIndex)
            {
                case 0:
                    return new KiemTien(this);
                case 1:
                    return new Events(this);
                case 2:
                    if (mAppStatus.Equals("1"))
                    {
                        return new TopKiemTien(true);
                    }
                    else
                    {
                        mDoiThuong = new DoiThuong(this);
                        return mDoiThuong;
                    }
                case 3:
                    return new ThongBao(this);
                case 4:
                    return new CaNhan(this);
                default:
                    return new KiemTien(this);
            }
        }

        public void setNotification(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Equals("0"))
            {
                mNotification.Visibility = Visibility.Collapsed;
            }
            else
            {
                mNotificationNumber.Text = value;
                mNotification.Visibility = Visibility.Visible;
            }
        }

        private void showActionbar(bool isShow)
        {
            if (isShow)
            {
                Toolbar.Visibility = Visibility.Visible;
            }
            else
            {
                Toolbar.Visibility = Visibility.Visible;
            }
        }

        public void setToolbarMoney(string coin)
        {
            ObjLogin mObjLogin = SharePreference.getUserInfo();
            try
            {
                if (mObjLogin != null)
                    mObjLogin.data.coin = coin;
                var json = JsonConvert.SerializeObject(mObjLogin);
                SharePreference.saveUserInfo(json);
            }
            catch (Exception e)
            {
            }
            ToolbarMoney.Text = convertCoin(coin);
        }

        public static String convertCoin(String coin)
        {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            try
            {
                float fCoin = float.Parse(coin);
                if (fCoin > 1000000 - 1) coin = (fCoin / 1000000).ToString("##0.0", nfi) + "M";
                else if (fCoin > 1000 - 1) coin = (fCoin / 1000).ToString("##0.0", nfi) + "K";
                return coin.Replace(".0", "");
            }
            catch (Exception e)
            {
                return coin.Replace(".0", "");
            }
        }

        public void gotoNewPage(string pageName)
        {
            NavigationService.Navigate(new Uri(pageName, UriKind.Relative));
        }

        private void nav_tab1_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Pivot.SelectedIndex = 0;
            selectedTab1();
        }
        private void nav_tab2_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Pivot.SelectedIndex = 1;
            selectedTab2();
        }
        private void nav_tab3_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Pivot.SelectedIndex = 2;
            selectedTab3();
        }
        private void nav_tab4_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Pivot.SelectedIndex = 3;
            selectedTab4();
        }
        private void nav_tab5_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Pivot.SelectedIndex = 4;
            selectedTab5();
        }

        public void selectedTab1()
        {
            nav_tab1.Background = new SolidColorBrush((App.Current.Resources["MainColor"] as SolidColorBrush).Color);
            nav_tab2.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab3.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab4.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab5.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);

            nav1_text.Foreground = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav2_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav3_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav4_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav5_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);

            img_tab1.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab1_active.png", UriKind.RelativeOrAbsolute));
            img_tab2.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab2.png", UriKind.RelativeOrAbsolute));
            if (mAppStatus.Equals("1"))
                img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/ico_top.png", UriKind.RelativeOrAbsolute));
            else
                img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab3.png", UriKind.RelativeOrAbsolute));
            img_tab4.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab4.png", UriKind.RelativeOrAbsolute));
            img_tab5.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab5.png", UriKind.RelativeOrAbsolute));
        }

        public void selectedTab2()
        {
            nav_tab2.Background = new SolidColorBrush((App.Current.Resources["MainColor"] as SolidColorBrush).Color);
            nav_tab1.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab3.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab4.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab5.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);

            nav2_text.Foreground = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav1_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav3_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav4_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav5_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);

            img_tab1.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab1.png", UriKind.RelativeOrAbsolute));
            img_tab2.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab2_active.png", UriKind.RelativeOrAbsolute));
            if (mAppStatus.Equals("1"))
                img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/ico_top.png", UriKind.RelativeOrAbsolute));
            else
                img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab3.png", UriKind.RelativeOrAbsolute));
            img_tab4.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab4.png", UriKind.RelativeOrAbsolute));
            img_tab5.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab5.png", UriKind.RelativeOrAbsolute));
        }

        public void selectedTab3()
        {
            nav_tab3.Background = new SolidColorBrush((App.Current.Resources["MainColor"] as SolidColorBrush).Color);
            nav_tab1.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab2.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab4.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab5.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);

            nav3_text.Foreground = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav1_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav2_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav4_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav5_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);

            img_tab1.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab1.png", UriKind.RelativeOrAbsolute));
            img_tab2.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab2.png", UriKind.RelativeOrAbsolute));
            if (mAppStatus.Equals("1"))
                img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/ico_top.png", UriKind.RelativeOrAbsolute));
            else
                img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab3_active.png", UriKind.RelativeOrAbsolute));
            img_tab4.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab4.png", UriKind.RelativeOrAbsolute));
            img_tab5.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab5.png", UriKind.RelativeOrAbsolute));
        }

        public void selectedTab4()
        {
            nav_tab4.Background = new SolidColorBrush((App.Current.Resources["MainColor"] as SolidColorBrush).Color);
            nav_tab1.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab2.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab3.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab5.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);

            nav4_text.Foreground = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav1_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav2_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav3_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav5_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);

            img_tab1.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab1.png", UriKind.RelativeOrAbsolute));
            img_tab2.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab2.png", UriKind.RelativeOrAbsolute));
            if (mAppStatus.Equals("1"))
                img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/ico_top.png", UriKind.RelativeOrAbsolute));
            else
                img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab3.png", UriKind.RelativeOrAbsolute));
            img_tab4.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab4_active.png", UriKind.RelativeOrAbsolute));
            img_tab5.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab5.png", UriKind.RelativeOrAbsolute));
        }

        public void selectedTab5()
        {
            nav_tab5.Background = new SolidColorBrush((App.Current.Resources["MainColor"] as SolidColorBrush).Color);
            nav_tab1.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab2.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab3.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav_tab4.Background = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);

            nav5_text.Foreground = new SolidColorBrush((App.Current.Resources["White"] as SolidColorBrush).Color);
            nav1_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav2_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav3_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);
            nav4_text.Foreground = new SolidColorBrush((App.Current.Resources["DarkGray"] as SolidColorBrush).Color);

            img_tab1.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab1.png", UriKind.RelativeOrAbsolute));
            img_tab2.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab2.png", UriKind.RelativeOrAbsolute));
            if (mAppStatus.Equals("1"))
                img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/ico_top.png", UriKind.RelativeOrAbsolute));
            else
                img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab3.png", UriKind.RelativeOrAbsolute));
            img_tab4.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab4.png", UriKind.RelativeOrAbsolute));
            img_tab5.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab5_active.png", UriKind.RelativeOrAbsolute));
        }

        public void sendDeviceInfo(string type, string content)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            byte[] deviceId = (byte[])DeviceExtendedProperties.GetValue("DeviceUniqueId");
            string deviceIDAsString = Convert.ToBase64String(deviceId);
            String version = Environment.OSVersion.ToString();
            String userId = "";
            ObjLogin objLogin = SharePreference.getUserInfo();
            if (objLogin != null) userId = objLogin.data.publisher_user_id;
            var nameHelper = new AssemblyName(Assembly.GetExecutingAssembly().FullName);
            var appVersion = nameHelper.FullName;
            //User Id
            param.Add(ApiKey.PUBLISHER_USER_ID, userId);
            //Thiet bi
            param.Add(ApiKey.DEVICE_MODEL, DeviceStatus.DeviceName);
            //Nha Mang
            param.Add(ApiKey.NETWORK_PROVIDER, DeviceNetworkInformation.CellularMobileOperator);
            //ip
            param.Add(ApiKey.DEVICE_IP, "noneed");
            //imei
            param.Add(ApiKey.DEVICE_IMEI, deviceIDAsString);
            //MAC
            param.Add(ApiKey.DEVICE_MAC_ADDRESS, "");
            //isWifiEnable
            param.Add(ApiKey.IS_WIFI_ENABLED, DeviceNetworkInformation.IsWiFiEnabled.ToString());
            //OS
            param.Add(ApiKey.OS, "1");
            //OS version
            param.Add(ApiKey.DEVICE_VERSION, version);
            //Tui3gang version
            param.Add(ApiKey.APP_VERSION, appVersion);
            //Thao tac thuc hien
            param.Add(ApiKey.ACTION, type);
            //Noi dung
            param.Add(ApiKey.CONTENT, content);
            //seri
            param.Add(ApiKey.DEVICE_SERI, deviceIDAsString);
            //manufactory
            param.Add(ApiKey.DEVICE_MANUFACTORY, DeviceStatus.DeviceManufacturer.ToString());
            BaseRequest.post(ApiUrl.URL_LOG, param, null, (result) =>
            {

            }, (error) =>
            {

            });
        }

        private void getAppStatus()
        {
            BaseRequest.get(ApiUrl.URL_APP_STATUS, null, null, (stream) =>
             {
                 var resp = new StreamReader(stream).ReadToEnd();
                 JObject obj = JObject.Parse(resp);
                 mAppStatus = (string)obj.SelectToken("status");
                 SharePreference.saveString(SharePreference.PREF_APP_STATUS, mAppStatus);
                 Dispatcher.BeginInvoke(() =>
                 {
                     if (mAppStatus.Equals("1"))
                     {
                         img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/ico_top.png", UriKind.RelativeOrAbsolute));
                         nav3_text.Text = "Hạng";
                     }
                     else
                     {
                         img_tab3.Source = new BitmapImage(new Uri(@"Assets/xhdpi/nav_tab3.png", UriKind.RelativeOrAbsolute));
                         nav3_text.Text = "Đổi thưởng";
                     }
                 });

             }, (error) =>
             {
                 Debug.WriteLine(error);
             });
        }

    }

}