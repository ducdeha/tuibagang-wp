﻿using System;

using Microsoft.Phone.Controls;

using System.Diagnostics;
using PivotContentDemo.Logins;
using System.Threading;
using Microsoft.Phone.Notification;
using System.Windows;
using System.Text;

namespace PivotContentDemo
{
    public partial class SplashScreen : PhoneApplicationPage
    {
        ObjLogin mObjLogin;
        public SplashScreen()
        {
            InitializeComponent();
            mObjLogin = SharePreference.getUserInfo();
            if (mObjLogin == null)
                setUpPushNotification();
        }

        private void splash_Completed(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
            {
                if (mObjLogin != null)
                {
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                    return;
                }
                else NavigationService.Navigate(new Uri("/Logins/Login.xaml", UriKind.Relative));
            });
        }

        private void setUpPushNotification()
        {
            HttpNotificationChannel pushChannel;
            string channelName = "TuiBaGang";
            pushChannel = HttpNotificationChannel.Find(channelName);
            if (pushChannel == null)
            {
                pushChannel = new HttpNotificationChannel(channelName);
                pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);
                pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);
                try
                {
                    pushChannel.Open();
                    pushChannel.BindToShellToast();
                }
                catch (Exception e) { }
            }
            else
            {
                pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);
                pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);
                Debug.WriteLine(pushChannel.ChannelUri.ToString());
                SharePreference.saveString(SharePreference.PREF_GCM_DEVICE_TOKEN, pushChannel.ChannelUri.ToString());
            }
        }
        void PushChannel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {

            Dispatcher.BeginInvoke(() =>
            {
                Debug.WriteLine(e.ChannelUri.ToString());
                SharePreference.saveString(SharePreference.PREF_GCM_DEVICE_TOKEN, e.ChannelUri.ToString());

            });
        }
        void PushChannel_ErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
        {
            Debug.WriteLine("Loi cmnr: Khong dang ky dc push uri");
        }
        void PushChannel_ShellToastNotificationReceived(object sender, NotificationEventArgs e)
        {
            StringBuilder message = new StringBuilder();
            string relativeUri = string.Empty;
            message.AppendFormat("Received Toast {0}:\n", DateTime.Now.ToShortTimeString());
            foreach (string key in e.Collection.Keys)
            {
                message.AppendFormat("{0}: {1}\n", key, e.Collection[key]);

                if (string.Compare(
                    key,
                    "wp:Param",
                    System.Globalization.CultureInfo.InvariantCulture,
                    System.Globalization.CompareOptions.IgnoreCase) == 0)
                {
                    relativeUri = e.Collection[key];
                }
            }
            Dispatcher.BeginInvoke(() => MessageBox.Show(message.ToString()));
        }
    }
}