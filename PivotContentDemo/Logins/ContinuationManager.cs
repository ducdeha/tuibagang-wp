﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace PivotContentDemo.Logins
{
    class ContinuationManager
    {
        public void ContinueWith(PhoneApplicationFrame frame, IActivatedEventArgs args)
        {
      
            if (frame == null)
                return;
            
            switch (args.Kind)
            {
                case ActivationKind.PickFileContinuation:
                    break;
                case ActivationKind.PickFolderContinuation:
                    break;
                case ActivationKind.PickSaveFileContinuation:
                    break;
                case ActivationKind.WebAuthenticationBrokerContinuation:
                    var continuator = frame.Content as IWebAuthenticationBrokerContinuable;
                    if (continuator != null)
                        continuator.ContinueWithWebAuthenticationBroker((WebAuthenticationBrokerContinuationEventArgs)args);
                    break;
                default:
                    break;
            }
            }
    }
    interface IWebAuthenticationBrokerContinuable
    {
        void ContinueWithWebAuthenticationBroker(WebAuthenticationBrokerContinuationEventArgs args);
    }
}
