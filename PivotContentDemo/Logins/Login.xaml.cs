﻿using System;
using System.Windows.Controls;
using System.Windows;
using Microsoft.Phone.Controls;
using Facebook;
using Facebook.Client;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Phone.Info;
using PivotContentDemo.Request;
using PivotContentDemo.Logins;
using Windows.ApplicationModel.Activation;

namespace PivotContentDemo
{
    public partial class Login : PhoneApplicationPage, IWebAuthenticationBrokerContinuable
    {
        private string AppId = "1085347091536431";
        private string mDob = "";
        private int mCityId = 17;
        private string mSocialId;
        private int mGender = 1;
        private string mAvatarUrl;
        public delegate void RESTSuccessCallback(Stream stream);
        public delegate void RESTErrorCallback(String reason);
        FaceBookHelper ObjFBHelper = new FaceBookHelper();

        public Login()
        {
            this.InitializeComponent();
            ScrollView.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
            setNgaySinh();
            loadInfo();
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Terminate();
            base.OnBackKeyPress(e);
        }


        private void FacebookBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ObjFBHelper.LoginAndContinue();
        }
      private   GraphUser mFacebookInfo;
        private async void loadInfo()
        {
            string accessToken = SharePreference.getString(SharePreference.PREF_FB_ACCESS_TOKEN);
            if (accessToken.Length >0)
            {
                try
                {
                    Progressbar.Visibility = Visibility.Visible;
                    var fb = new FacebookClient(accessToken);
                    dynamic result = await fb.GetTaskAsync("me");
                     mFacebookInfo = new GraphUser(result);
                    mSocialId = mFacebookInfo.Id;
                    callApiLogin(true);                    
                }
                catch (FacebookOAuthException exception)
                {
                    MessageBox.Show("Bạn không thể đăng nhập ở thời điểm hiện tại. Vui lòng thử lại sau.");
                }

            }
        }

        private void Email_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Email.Text.Equals("Nhập email")) Email.Text = "";
            Email.Focus();
            ScrollView.Margin = new Thickness(0, 0, 0, 400);
            Email.Foreground = new SolidColorBrush((App.Current.Resources["Black"] as SolidColorBrush).Color);
            ScrollView.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            ScrollView.Measure(ScrollView.RenderSize);
            ScrollView.ScrollToVerticalOffset(ScrollView.ScrollableHeight);
        }

        private void Email_LostFocus(object sender, RoutedEventArgs e)
        {
            String email = Email.Text;
            if (String.IsNullOrEmpty(email))
            {
                Email.Text = "Nhập email";
                Email.Foreground = new SolidColorBrush((App.Current.Resources["HintColor"] as SolidColorBrush).Color);
            }
            ScrollView.Margin = new Thickness(0, 0, 0, 0);
            ScrollView.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
        }

        private void NgaySinh1_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            setNgaySinh();
        }

        private void setNgaySinh()
        {
            var date = NgaySinh1.Value.Value.Day;
            var year = NgaySinh1.Value.Value.Year;
            var month = NgaySinh1.Value.Value.Month;
            mDob = date + "/" + month + "/" + year;
            NgaySinh.Text = mDob;
        }
        ListPicker listPicker;
        private void ThanhPho_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            listPicker = new ListPicker()
            {
                Header = "Chọn thành phố:",
                ItemsSource = new string[] { "An Giang", "Bà Rịa - Vũng Tầu", "Bình", "Dương Bình Phước", "Bình Thuận", "Bình Định", "Bắc Giang", "Bắc Kạn", "Bắc Ninh", "Bến Tre", "Cao Bằng", "Cà Mau", "Cần Thơ", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Nội", "Điện Biên", "Hà Tĩnh", "Hòa Bình", "Hưng Yên", "Hải Dương", "Hải Phòng", "Hồ Chí Minh", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Long An", "Lào Cai", "Lâm Đồng", "Lạng Sơn", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Phú Yên", "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sơn La", "Thanh Hóa", "Thái Bình", "Thái Nguyên", "Thừa Thiên - Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Tây Ninh", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái", "Đà Nẵng", "Đắk Lắk", "Đồng Nai", "Đồng Tháp", "Bạc Liêu", "Sóc Trăng", "Hậu Giang", "Đắk Nông" },
                Margin = new Thickness(12, 42, 24, 18)
            };
            listPicker.SetValue(ListPicker.SelectedIndexProperty, 16);
            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Content = listPicker,
                LeftButtonContent = "Ok",
                IsFullScreen = false,
            };
            messageBox.Dismissing += (s1, e1) =>
            {
                if (listPicker.ListPickerMode == ListPickerMode.Expanded)
                {
                    //e1.Cancel = true;
                }
            };

            messageBox.Dismissed += (s2, e2) =>
            {
                switch (e2.Result)
                {
                    case CustomMessageBoxResult.LeftButton:

                        break;
                    case CustomMessageBoxResult.None:

                        break;
                    default:
                        break;
                }
            };
            listPicker.SelectionChanged += ListPicker_SelectionChanged;
            messageBox.Show();

        }

        private void ListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var picker = sender as ListPicker;
            ThanhPho.Text = picker.SelectedItem.ToString();
            mCityId = picker.SelectedIndex + 1;
        }

        private void Huybo(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SessionStorage.Remove();
            LoginView.Visibility = Visibility.Visible;
            ScrollView.Visibility = Visibility.Collapsed;
        }

        private void Tieptuc(object sender, System.Windows.Input.GestureEventArgs e)
        {
            callApiLogin(false);
        }
   
        public void addLoginParams(Dictionary<String, String> param)
        {
            String version = Environment.OSVersion.ToString();
            param.Add(ApiKey.DEVICE_VERSION, version );
            param.Add(ApiKey.DEVICE_IP, "Webservice will detect this");
            param.Add(ApiKey.DEVICE_MAC_ADDRESS, "");
            param.Add(ApiKey.DEVICE_SERI,getDeviceId());
        }

        private void callApiLogin(bool isFirtLogin)
        {
            string email = "", name = "", birthday = "";
            string gcmId = SharePreference.getString(SharePreference.PREF_GCM_DEVICE_TOKEN);
            email = Email.Text;
            name = TaiKhoan.Text;
            birthday = mDob;
            //DucNT: defaul la Ha Noi- 17
            if (mCityId == 0) mCityId = 17;           
            var manufacturer = DeviceStatus.DeviceManufacturer;
            string mnf = manufacturer.ToString();
            string deviceName = DeviceStatus.DeviceName;
            byte[] deviceId = (byte[])DeviceExtendedProperties.GetValue("DeviceUniqueId");
            string deviceIDAsString = Convert.ToBase64String(deviceId);
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add(ApiKey.PUBLISHER_SOCIAL_ID, mSocialId);
            if (!isFirtLogin)
            {
                bool trueEmail = IsValidEmail(email);
                StringBuilder error = new StringBuilder();
                if (string.IsNullOrEmpty(name))
                    error.Append("Tài khoản không được để trống.").Append("\n");
                if (string.IsNullOrEmpty(email) || email.Equals("Nhập email"))
                    error.Append("Email không được để trống.").Append("\n");
                else
                if (!trueEmail) error.Append("Email không hợp lệ").Append("\n");
                if (string.IsNullOrEmpty(birthday))
                    error.Append("Ngày sinh không được để trống.").Append("\n");
                if (!string.IsNullOrEmpty(error.ToString()))
                {
                    string txtError = error.ToString();
                    MessageBox.Show(txtError, "Lỗi", MessageBoxButton.OK);
                    return;
                }
                param.Add(ApiKey.PUBLISHER_EMAIL, email);
                param.Add(ApiKey.PUBLISHER_AVATAR_URL, mAvatarUrl);
                param.Add(ApiKey.PUBLISHER_FACEBOOK_NAME, name);
                param.Add(ApiKey.BIRTHDAY, birthday);
                param.Add(ApiKey.PUBLISHER_SEX, mGender + "");//DucNT: 1 la male     
                param.Add(ApiKey.PUBLISHER_CITY, mCityId + "");
            }        
            param.Add(ApiKey.DEVICE_MODEL, deviceName);
            param.Add(ApiKey.DEVICE_ID, deviceIDAsString);
            param.Add(ApiKey.DEVICE_MANUFACTORY, mnf);
            param.Add(ApiKey.GCM_ID, gcmId);
            param.Add(ApiKey.OS, "3");
            addLoginParams(param);
            Progressbar.Visibility = Visibility.Visible;

            BaseRequest.post(ApiUrl.URL_LOGIN, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                SharePreference.saveUserInfo(data);
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));

                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                  
                    if (reason.Contains("Page not found"))
                    {
                        mFacebookInfo.ProfilePictureUrl = new Uri(string.Format("https://graph.facebook.com/{0}/picture", mFacebookInfo.Id));
                        mAvatarUrl = string.Format("https://graph.facebook.com/{0}/picture", mFacebookInfo.Id);
                        Progressbar.Visibility = Visibility.Collapsed;
                        LoginView.Visibility = Visibility.Collapsed;
                        ScrollView.Visibility = Visibility.Visible;
                        ImageSource imgsrc1 = new BitmapImage(new Uri(mAvatarUrl, UriKind.RelativeOrAbsolute));
                        Avatar.ImageSource = imgsrc1;
                        TaiKhoan.Text = mFacebookInfo.Name;
                    }
                    else BaseRequest.showErrorBox(reason);
                });
            });
        }

        private String getDeviceId()
        {
            object uniqueId;
            var hexString = string.Empty;
            if (DeviceExtendedProperties.TryGetValue("DeviceUniqueId", out uniqueId))
                hexString = BitConverter.ToString((byte[])uniqueId).Replace("-", string.Empty);
            return hexString;
        }

        public bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn,
                   @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }

        FacebookClient fbclient = new FacebookClient();
        public void ContinueWithWebAuthenticationBroker(WebAuthenticationBrokerContinuationEventArgs args)
        {
            ObjFBHelper.ContinueAuthentication(args);
            if (ObjFBHelper.AccessToken != null)
            {
                SharePreference.saveString(SharePreference.PREF_FB_ACCESS_TOKEN, ObjFBHelper.AccessToken);
                var fb = new FacebookClient(ObjFBHelper.AccessToken);
                loadInfo();
            }
        }
    }
}
