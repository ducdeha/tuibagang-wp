﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PivotContentDemo.Logins
{
    public class ObjLogin
    {
        public Data data { get; set; }
    }
    
    public class Data
    {
        public string publisher_user_id { get; set; }
        public string publisher_social_id { get; set; }
        public string publisher_email { get; set; }
        public string publisher_avatar_url { get; set; }
        public string publisher_facebook_name { get; set; }
        public int publisher_age { get; set; }
        public string publisher_city { get; set; }
        public string publisher_sex { get; set; }
        public string coin { get; set; }
        public string share_code { get; set; }
        public string share_code_status { get; set; }
        public string accept_token { get; set; }
    }

  
}
