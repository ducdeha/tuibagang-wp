﻿namespace PivotContentDemo
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Markup;
    using System.Windows.Navigation;
    using Microsoft.Phone.Controls;
    using Microsoft.Phone.Shell;
    using PivotContentDemo.Resources;
    using Windows.ApplicationModel.Activation;
    using Logins;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using PivotContentDemo.Logins;
    public partial class App : System.Windows.Application
    {
        public static PhoneApplicationFrame RootFrame { get; private set; }
        public App()
        {
            this.UnhandledException += this.Application_UnhandledException;
            this.InitializeComponent();
            this.InitializePhoneApplication();
            this.InitializeLanguage();
            PhoneApplicationService.Current.ContractActivated += Application_ContractActivated;
            if (Debugger.IsAttached)
            {
                System.Windows.Application.Current.Host.Settings.EnableFrameRateCounter = true;
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }
        }

        ContinuationManager _continuator = new ContinuationManager();
        private async void Application_ContractActivated(object sender, IActivatedEventArgs args)
        {
          //  CreateRootFrame();
            if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
            {
                try
                {
                    await SuspensionManager.RestoreAsync();
                }
                catch { }
            }
            if (args is IContinuationActivatedEventArgs)
                _continuator.ContinueWith(RootFrame,args);
          //  Window.Current.Activate();
        }
        
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                //       Debugger.Break();
            }
        }
        
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                Debugger.Break();
            }
        }
        
        #region Phone application initialization
        private bool phoneApplicationInitialized = false;
        private void InitializePhoneApplication()
        {
            if (this.phoneApplicationInitialized)
                return;
            RootFrame = new PhoneApplicationFrame();
            RootFrame.Navigated += this.CompleteInitializePhoneApplication;
            RootFrame.NavigationFailed += this.RootFrame_NavigationFailed;
            RootFrame.Navigated += this.CheckForResetNavigation;
            RootFrame.UriMapper = new ContosoCallbackUriMapper();
            this.phoneApplicationInitialized = true;
        }
                
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            if (this.RootVisual != RootFrame)
                this.RootVisual = RootFrame;
            RootFrame.Navigated -= this.CompleteInitializePhoneApplication;
        }

        private void CheckForResetNavigation(object sender, NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Reset)
                RootFrame.Navigated += this.ClearBackStackAfterReset;
        }

        private void ClearBackStackAfterReset(object sender, NavigationEventArgs e)
        {
            RootFrame.Navigated -= this.ClearBackStackAfterReset;
            if (e.NavigationMode != NavigationMode.New && e.NavigationMode != NavigationMode.Refresh)
                return;
            while (RootFrame.RemoveBackEntry() != null)
            {
                ;
            }
        }
        #endregion

        private void InitializeLanguage()
        {
            try
            {
                RootFrame.Language = XmlLanguage.GetLanguage(AppResources.ResourceLanguage);
                System.Windows.FlowDirection flow = (System.Windows.FlowDirection)Enum.Parse(typeof(System.Windows.FlowDirection), AppResources.ResourceFlowDirection);
                RootFrame.FlowDirection = flow;
            }
            catch
            {
                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }

                throw;
            }
        }
        Frame rootFrame;
        private void CreateRootFrame()
        {
            rootFrame = Window.Current.Content as Frame;
            if (rootFrame == null)
            {
                rootFrame = new Frame();
                SuspensionManager.RegisterFrame(rootFrame, "AppFrame");
                Window.Current.Content = rootFrame;
            }
        }
    }
}