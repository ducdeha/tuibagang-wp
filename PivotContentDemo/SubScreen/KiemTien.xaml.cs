﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PivotContentDemo.Request;
using System.Threading.Tasks;
using System.Text;
using PivotContentDemo.Logins;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace PivotContentDemo.SubScreen
{
    public partial class KiemTien : UserControl
    {
        private MainPage mMainPage;
        private ObjLogin mObjLogin;
        private ArrApp mAppClicked;
        private ObservableCollection<ArrApp> mArrData
        {
            get;
            set;
        }

        private ObservableCollection<Guide> mArrGuide
        {
            get;
            set;
        }

        public KiemTien(MainPage mainpage)
        {
            mMainPage = mainpage;
            this.InitializeComponent();

            mArrData = new ObservableCollection<ArrApp>();
            mArrGuide = new ObservableCollection<Guide>();
            mObjLogin = SharePreference.getUserInfo();
            loadData(false);
        }


        private void PullupLoadmore_RefreshRequested(object sender, EventArgs e)
        {
            loadData(true);
        }

        private void refreshPanelRefreshRequested(object sender, EventArgs e)
        {
            loadData(false);
        }

        private void RefreshTab(object sender, System.Windows.Input.GestureEventArgs e)
        {
            loadData(false);
        }

        private void loadData(bool isLoadmore)
        {
            Progressbar.Visibility = Visibility;
            if (!isLoadmore)
            {
                mArrData.Clear();
            }
            Dictionary<String, String> param = new Dictionary<string, string>();
            param.Add(ApiKey.OFFSET, "" + mArrData.Count);
            param.Add(ApiKey.PUBLISHER_SEX, "" + mObjLogin.data.publisher_sex);
            param.Add(ApiKey.PUBLISHER_AGE, "" + mObjLogin.data.publisher_age);
            param.Add(ApiKey.PUBLISHER_CITY, "" + mObjLogin.data.publisher_city);
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            BaseRequest.get(ApiUrl.URL_GET_LIST_APP, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                RootObject rootObject = new RootObject();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(rootObject.GetType());
                rootObject = ser.ReadObject(ms) as RootObject;
                Dispatcher.BeginInvoke(() =>
                {
                    List<ArrApp> arrData = rootObject.arr_app;
                    if (arrData == null) return;
                    for (int i = 0; i < arrData.Count; i++)
                    {
                        ArrApp app = arrData[i];
                        setAppStatus(app);
                        app.selection = mArrData.Count;
                        mArrData.Add(arrData[i]);
                    }
                    list.ItemsSource = mArrData;
                    updateUI(mArrData);
                    Progressbar.Visibility = Visibility.Collapsed;
                    mMainPage.setToolbarMoney(rootObject.coin);
                    mMainPage.setNotification(rootObject.notification_count + "");
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    updateUI(null);
                    BaseRequest.showErrorBox(mMainPage, reason);

                });
            });
        }

        private void setAppStatus(ArrApp app)
        {
            int status = int.Parse(app.status);
            if (status == 0)
            {
                //DucNT: Chua cai dat
                app.status_text = "Chưa cài đặt";
                app.status_image = "/Assets/xhdpi/ico_download.png";
            }
            else if (status == 1)
            {
                //DucNT: Yêu cầu vào app để xác nhận cài đặt
                app.status_text = "Mở App để xác nhận cài đặt";
                app.status_image = "/Assets/xhdpi/ico_done.png";
            }
            else if (status == 4)
            {
                //DucNT: Da cai dat nhung chua nhan thuong
                app.status_text = "Chờ nhận thưởng";
                app.status_image = "/Assets/xhdpi/ico_done.png";

            }
            else if (status == 2)
            {
                //DucNT: Cho nhan thuong dot 2
                app.status_text = "Chờ nhận thưởng đợt 2";
                app.status_image = "/Assets/xhdpi/ico_done.png";
            }
            else
            {
                //DucNT: Nhan thuong xong xuoi
                app.status_text = "Đã hoàn thành";
                app.status_image = "/Assets/xhdpi/ico_done.png";
            }

        }

        private void updateUI(ObservableCollection<ArrApp> arrApp)
        {
            if (arrApp == null)
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không tải được";
            }
            else
           if (arrApp.Count > 0)
            {
                ContentView.Visibility = Visibility.Visible;
                ErrorView.Visibility = Visibility.Collapsed;
            }
            else
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không có dữ liệu";
            }
        }

        private void GuideDialog_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (!mIsBorderTap)
            {
                GuideDialog.Visibility = Visibility.Collapsed;
                mListView.IsHitTestVisible = true;
            }
            mIsBorderTap = false;
        }

        private void onItemClickListener(object sender, System.Windows.Input.GestureEventArgs e)
        {
            mListView.IsHitTestVisible = false;
            mIsBorderTap = false;
            mAppClicked = (ArrApp)((FrameworkElement)sender).DataContext;
            string status = mAppClicked.status;
            callApiClickItem(mAppClicked.campaign_id);
            //DucNT: Nếu đã hoàn thành thì click vào ko hiện ra gì cả
            if (status.Equals("3")) return;
            mArrGuide.Clear();
            try
            {
                mAppIcon.ImageSource = new BitmapImage(new Uri(mAppClicked.app_image, UriKind.Absolute));
            }
            catch (Exception ex) { }
            //DucNT: Bind data for guide dialog
            mAppTitle.Text = mAppClicked.app_name;
            mInstallCount.Text = "Còn lại: " + mAppClicked.install_count;
            mAppPrice.Text = "+" + mAppClicked.cost_per_install + " C";
            List<object> listSteps = mAppClicked.desc.arr_steps;
            for (int i = 0; i < listSteps.Count; i++)
            {
                Guide guide = new Guide();
                guide.content = listSteps[i].ToString();
                guide.position = i + 1;
                mArrGuide.Add(guide);
            }
            mListBox.ItemsSource = mArrGuide;
            mNotice.Text = "Chú ý: " + mAppClicked.desc.notice;
            if (status.Equals("0"))
                mSetupBtnName.Text = "Cài đặt";
            else if (status.Equals("1")) mSetupBtnName.Text = "Mở App"; else mSetupBtnName.Text = "Nhận thưởng";
            GuideDialog.Visibility = Visibility.Visible;
        }

        //Gọi api này để server thống kê lượt click
        private void callApiClickItem(string id)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            param.Add(ApiKey.PUBLISHER_CAMPAIGN_ID, id);
            BaseRequest.get(ApiUrl.URL_CLICK_APP, param, null, null, null);
        }

        private void mBackBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            GuideDialog.Visibility = Visibility.Collapsed;
            mListView.IsHitTestVisible = true;
        }

        private async void mSetupBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            mListView.IsHitTestVisible = true;
            GuideDialog.Visibility = Visibility.Collapsed;
            int status = int.Parse(mAppClicked.status);
            switch (status)
            {
                case 0:
                    //Chua cai dat                  
                    callApiInstallApp();
                    break;
                case 1:
                    //Đã bấm nút cài đặt còn cài hay chưa cài thì ko biết - Xac nhan cai dat
                    bool canOpen = false;
                    try
                    {
                        canOpen = await Windows.System.Launcher.LaunchUriAsync(new Uri(mAppClicked.url_scheme));
                    }
                    catch (Exception ee) { }
                    if (canOpen)
                    {
                        callApiAppInstalledConfirm();
                    }
                    else MessageBox.Show("Đường dẫn tới App không hợp lệ. Vui lòng liên hệ  với admin để biết thêm chi tiết", "Lỗi", new MessageBoxButton());

                    break;
                case 4:
                    //Đã cài thật sự, có thể nhận thưởng đợt 1
                    callApiGetCoin();
                    break;
                case 2:
                    callApiGetCoin();
                    //Chờ nhận thưởng đợt 2
                    break;
                case 3:
                    //Đã cài thành công
                    break;

            }
        }



        //DucNT: Cài đặt app
        private void callApiInstallApp()
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            param.Add(ApiKey.PUBLISHER_CAMPAIGN_ID, mAppClicked.campaign_id);
            Progressbar.Visibility = Visibility.Visible;
            BaseRequest.post(ApiUrl.URL_INSTALL_APP, param, null, (stream) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    mMainPage.sendDeviceInfo("Tải App", "Tải ứng dụng: " + mAppClicked.app_name);
                    updateRow("1");
                    Progressbar.Visibility = Visibility.Collapsed;
                    openAppOnStore(mAppClicked, mAppClicked.cus_option_install == 0);

                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    BaseRequest.showErrorBox(reason);
                });
            });
        }

        private void updateRow(string status)
        {
            mAppClicked.status = status;
            setAppStatus(mAppClicked);
            mArrData[mAppClicked.selection] = mAppClicked;
            list.ItemsSource = mArrData;
        }

        private int mConfirmCount = 0;
        //Xác nhận cài đặt app
        private void callApiAppInstalledConfirm()
        {

            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add(ApiKey.CONTENT, mAppClicked.campaign_id);
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            Progressbar.Visibility = Visibility.Visible;
            BaseRequest.get(ApiUrl.URL_CHECK_INSTALL, param, null, (stream) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    updateRow("4");
                    mConfirmCount = 0;
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    if (string.IsNullOrEmpty(reason))
                    {
                        if (mConfirmCount > 3)
                        {
                            mConfirmCount = 0;
                        }
                        else
                        {
                            callApiAppInstalledConfirm();
                            mConfirmCount = mConfirmCount + 1;
                        }
                    }
                    else
                    {
                        mConfirmCount = 0;
                        Progressbar.Visibility = Visibility.Collapsed;
                        BaseRequest.showErrorBox(reason);
                    }
                });
            });
        }

        //Nhận thưởng
        private void callApiGetCoin()
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            param.Add(ApiKey.PUBLISHER_CAMPAIGN_ID, mAppClicked.campaign_id);
            Progressbar.Visibility = Visibility.Visible;
            BaseRequest.post(ApiUrl.URL_GET_COIN, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                Dispatcher.BeginInvoke(() =>
                {
                    mMainPage.sendDeviceInfo("Nhận thưởng", "Nhận thưởng từ ứng dụng: " + mAppClicked.app_name);
                    Progressbar.Visibility = Visibility.Collapsed;
                    JObject o = JObject.Parse(data);
                    bool result = (bool)o.SelectToken("result");
                    string coin = (string)o.SelectToken("coin");
                    string msg = (string)o.SelectToken("message");
                    string status = (string)o.SelectToken("status");
                    mMainPage.setToolbarMoney(coin);
                    updateRow(status);
                    if (!string.IsNullOrEmpty(msg)) MessageBox.Show(msg, "Thông báo", new MessageBoxButton());
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    BaseRequest.showErrorBox(reason);
                });
            });



        }

        private async void openAppOnStore(ArrApp input, bool isSearch)
        {
            if (isSearch)
            {
                var uri = new Uri(string.Format("ms-windows-store:search?keyword={0}", input.key_word));
                await Windows.System.Launcher.LaunchUriAsync(uri);
            }
            else
            {
                var uri = new Uri(string.Format("ms-windows-store:navigate?appid={0}", input.app_package));
                await Windows.System.Launcher.LaunchUriAsync(uri);
            }
        }
        private bool mIsBorderTap;
        private void Border_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            mIsBorderTap = true;
        }
    }

    public class Desc
    {
        public List<object> arr_steps { get; set; }
        public string notice { get; set; }
    }

    public class Guide
    {
        public int position { get; set; }
        public string content { get; set; }
    }

    public class ArrApp
    {
        public string campaign_id { get; set; }
        public string app_package { get; set; }
        public string app_name { get; set; }
        public string key_word { get; set; }
        public string app_image { get; set; }
        public int cost_per_install { get; set; }
        public string status { get; set; }
        public string app_url { get; set; }
        public string url_scheme { get; set; }
        public int install_count { get; set; }
        public string status_text { get; set; }
        public string status_image { get; set; }
        public int cus_option_install { get; set; }
        public Desc desc { get; set; }
        public int selection { get; set; }
    }

    public class RootObject
    {
        public string coin { get; set; }
        public List<ArrApp> arr_app { get; set; }
        public int notification_count { get; set; }
    }
}
