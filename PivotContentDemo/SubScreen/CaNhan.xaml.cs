﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PivotContentDemo.Logins;
using System.Windows.Media.Imaging;
using PivotContentDemo.SubScreen;
using System.Threading;
using Windows.UI.Core;
using System.Windows.Media;
using PivotContentDemo.Resources;
using PivotContentDemo.Request;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;

namespace PivotContentDemo
{
    public partial class CaNhan : UserControl
    {
        private string mAppStatus = "0";
        private MainPage mMainPage;
        private ObjLogin mObjLogin;
        private BitmapImage mBackgroundBitmap;
        public CaNhan(MainPage mainpage)
        {
            this.InitializeComponent();
            mAppStatus = SharePreference.getString(SharePreference.PREF_APP_STATUS);
            if (mAppStatus.Equals("1"))
            {
                mLsNhanThuong.Visibility = Visibility.Collapsed;
                mTopRank.Visibility = Visibility.Collapsed;
            }else
            {
                mLsNhanThuong.Visibility = Visibility.Visible;
                mTopRank.Visibility = Visibility.Visible;
            }
            mMainPage = mainpage;
            mObjLogin = SharePreference.getUserInfo();
            mAvatar.ImageSource = new BitmapImage(new Uri(mObjLogin.data.publisher_avatar_url));
            //mBackground.Source = new BitmapImage(new Uri(mObjLogin.data.publisher_avatar_url));
            Uri uri = new Uri(mObjLogin.data.publisher_avatar_url, UriKind.Absolute);
            mBackgroundBitmap = new BitmapImage(uri);
            mUserName.Text = mObjLogin.data.publisher_facebook_name;
            mCoin.Text = DoiThuong.convertCoin(mObjLogin.data.coin) + " Coin";
            mShareCode.Text = mObjLogin.data.share_code;
            string shareStatus = mObjLogin.data.share_code_status;
            if (shareStatus.Equals("1"))
            {
                mNhapMa.Visibility = Visibility.Collapsed;
            }
            else mNhapMa.Visibility = Visibility.Visible;
            makeBlurImage();
            ShareSourceLoad();
        }

        private void makeBlurImage()
        {
            Uri uri = new Uri(mObjLogin.data.publisher_avatar_url, UriKind.Absolute);
            BitmapImage background = new BitmapImage(uri);
            background.CreateOptions = BitmapCreateOptions.None;
            background.ImageOpened += (s, e) =>
            {
                WriteableBitmap bitmap = new WriteableBitmap(background);
                var blurredBitmap = WriteableBitmapExtensions.Convolute(bitmap,
                          WriteableBitmapExtensions.KernelGaussianBlur5x5);
                mBackground.Source = blurredBitmap;
            };

        }

        private void mNhapMa_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            
            TextBox textbox = new TextBox();
            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Content = textbox,
                Title = "Nhập mã",
                Message = "Bạn vui lòng nhập mã chia sẻ từ bạn bè để nhận thưởng",
                LeftButtonContent = "Xong",
                RightButtonContent = "Hủy bỏ",
                IsFullScreen = false,
            };
            messageBox.Dismissed += (s2, e2) =>
            {
                switch (e2.Result)
                {
                    case CustomMessageBoxResult.LeftButton:
                        callApiInputShareCode(textbox.Text);
                        break;
                    case CustomMessageBoxResult.RightButton:

                        break;
                    case CustomMessageBoxResult.None:

                        break;
                    default:
                        break;
                }
            };
            messageBox.Show();
        }

        private void mLsNhanThuong_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            mMainPage.gotoNewPage("/SubScreen/SubCaNhan/LsNhanThuong.xaml");
        }

        private void mLsHoatDong_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            mMainPage.gotoNewPage("/SubScreen/SubCaNhan/LsHoatDong.xaml");
        }

        private void mTopRank_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            mMainPage.gotoNewPage("/SubScreen/SubCaNhan/TopKiemTien.xaml");
        }

        private void mCauHoi_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            mMainPage.gotoNewPage("/SubScreen/SubCaNhan/CauHoiThuongGap.xaml");
        }

        private void mLienHe_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            mMainPage.gotoNewPage("/SubScreen/SubCaNhan/LienHe.xaml");
        }

        private void mGioiThieu_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            mMainPage.gotoNewPage("/SubScreen/SubCaNhan/GioiThieu.xaml");
        }

        private void Chiase(object sender, System.Windows.Input.GestureEventArgs e)
        {
            DataTransferManager.ShowShareUI();
        }

        private void callApiInputShareCode(String text)
        {
            if (string.IsNullOrEmpty(text)) return;
            Dictionary<String, String> param = new Dictionary<String, String>();
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            param.Add(ApiKey.SHARE_CODE, text);
            Progressbar.Visibility = Visibility.Visible;
            BaseRequest.get(ApiUrl.URL_SHARE_CODE, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                SharePreference.saveUserInfo(data);
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;

                    mNhapMa.Visibility = Visibility.Collapsed;
                    //Cap nhat object objLogin
                    mObjLogin.data.share_code_status = "1";
                    var json = JsonConvert.SerializeObject(mObjLogin);
                    SharePreference.saveUserInfo(json);
                    JObject o = JObject.Parse(data);
                    string msg = (string)o.SelectToken("message");
                    string coin = (string)o.SelectToken("coin");
                    mMainPage.setToolbarMoney(coin);
                    mCoin.Text = DoiThuong.convertCoin(coin);

                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    BaseRequest.showErrorBox(reason);
                   
                });
            });
        }
        
        private void ShareSourceLoad()
        {
            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
            dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.DataRequested);
        }

        private void DataRequested(DataTransferManager sender, DataRequestedEventArgs e)
        {
            DataRequest request = e.Request;
            request.Data.Properties.Title = "Chia sẻ: ";
            request.Data.Properties.Description = "";
            string shareContent =string.Format("Hãy nhanh tay cài đặt ứng dụng Túi Ba Gang để nhận thẻ cào điện thoại nào các bạn ơi. Nhập mã {0} để nhận quà khi đăng nhập. Vui lòng truy cập http://tui3gang.vn/ để tải ứng dụng.", mObjLogin.data.share_code);
            request.Data.SetText(shareContent);
        }
    }
}
