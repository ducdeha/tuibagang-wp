﻿
using PivotContentDemo.Logins;
using PivotContentDemo.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Globalization;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;

namespace PivotContentDemo.SubScreen
{
    public partial class DoiThuong : UserControl
    {
        private ObjLogin mObjLogin;
        private MainPage mMainPage;

        private ObservableCollection<Viettel> mArrVietel
        {
            get;
            set;
        } = new ObservableCollection<Viettel>();
        private ObservableCollection<MobiFone> mArrMobi
        {
            get;
            set;
        } = new ObservableCollection<MobiFone>();
        private ObservableCollection<Vinaphone> mArrVina
        {
            get;
            set;
        } = new ObservableCollection<Vinaphone>();

        public DoiThuong(MainPage mainpage)
        {
            this.InitializeComponent();
            mMainPage = mainpage;
            mObjLogin = SharePreference.getUserInfo();
            loadData(false);
        }

        private void RefreshTab(object sender, System.Windows.Input.GestureEventArgs e)
        {
            loadData(false);
        }
        public void loadData(bool isLoadmore)
        {
            Progressbar.Visibility = Visibility;
            if (!isLoadmore)
            {
                mArrVietel.Clear();
                mArrMobi.Clear();
                mArrVina.Clear();
            }
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            BaseRequest.get(ApiUrl.URL_GET_LIST_CARD, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                ObjDoiThuong objDoiThuong = new ObjDoiThuong();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(objDoiThuong.GetType());
                objDoiThuong = ser.ReadObject(ms) as ObjDoiThuong;
                Dispatcher.BeginInvoke(() =>
                {
                    string coin = convertCoin(objDoiThuong.coin);
                    mMainPage.setToolbarMoney(objDoiThuong.coin);
                    mCoin.Text = string.Format("Số tiền có thể rút: {0} C (1C~1VNĐ)", coin);
                    ArrCards arrData = objDoiThuong.arr_cards;
                    List<Viettel> arrVietel = arrData.Viettel;
                    if (arrVietel != null)
                    {
                        for (int i = 0; i < arrVietel.Count; i++)
                        {
                            Viettel app = arrVietel[i];
                            app.mobile_card_price = convertCoin(app.mobile_card_price);
                            mArrVietel.Add(app);
                        }
                        mListVietel.ItemsSource = null;
                        mListVietel.ItemsSource = mArrVietel;
                    }
                    //Mobi
                    List<MobiFone> arrMobi = arrData.MobiFone;
                    if (arrMobi != null)
                    {
                        for (int i = 0; i < arrMobi.Count; i++)
                        {
                            MobiFone app = arrMobi[i];
                            app.mobile_card_price = convertCoin(app.mobile_card_price);
                            mArrMobi.Add(app);
                        }
                        mListMobi.ItemsSource = null;
                        mListMobi.ItemsSource = mArrMobi;
                    }
                    //Vina
                    List<Vinaphone> arrVina = arrData.Vinaphone;
                    if (arrVina != null)
                    {
                        for (int i = 0; i < arrVina.Count; i++)
                        {
                            Vinaphone app = arrVina[i];
                            app.mobile_card_price = convertCoin(app.mobile_card_price);
                            mArrVina.Add(app);
                        }
                        mListVina.ItemsSource = null;
                        mListVina.ItemsSource = mArrVina;
                    }
                    updateUI(mArrVietel, mArrMobi, mArrVina);
                    Progressbar.Visibility = Visibility.Collapsed;
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    updateUI(null, null, null);
                    BaseRequest.showErrorBox(mMainPage, reason);
                });
            });
        }

        private void updateUI(ObservableCollection<Viettel> arrVietel, ObservableCollection<MobiFone> arrMobi, ObservableCollection<Vinaphone> arrVina)
        {
            if (arrVietel == null && arrMobi == null && arrVina == null)
            {
                {
                    ContentView.Visibility = Visibility.Collapsed;
                    ErrorView.Visibility = Visibility.Visible;
                    ErrorViewText.Text = "Không tải được";
                    return;
                }
            }
            else
            {
                ContentView.Visibility = Visibility.Visible;
                ErrorView.Visibility = Visibility.Collapsed;
            }

            //Vietel
            if (arrVietel == null || arrVietel.Count == 0)
            {
                mTitleVietel.Visibility = Visibility.Collapsed;
                mListVietel.Visibility = Visibility.Collapsed;
            }
            else
            {
                {
                    mTitleVietel.Visibility = Visibility.Visible;
                    mListVietel.Visibility = Visibility.Visible;
                }
            }

            //Mobi
            if (arrMobi == null || arrMobi.Count == 0)
            {
                mTitleMobi.Visibility = Visibility.Collapsed;
                mListMobi.Visibility = Visibility.Collapsed;
            }
            else
            {
                {
                    mTitleMobi.Visibility = Visibility.Visible;
                    mListMobi.Visibility = Visibility.Visible;
                }
            }

            //Vina
            if (arrVina == null || arrVina.Count == 0)
            {
                mTitleVina.Visibility = Visibility.Collapsed;
                mListVina.Visibility = Visibility.Collapsed;
            }
            else
            {
                {
                    mTitleVina.Visibility = Visibility.Visible;
                    mListVina.Visibility = Visibility.Visible;
                }
            }


        }

        public static string convertCoin(String coin)
        {
            //DucNT: Convert tien tu trang nay
            //https://books.google.com.vn/books?id=HMowCwAAQBAJ&pg=PT123&lpg=PT123&dq=format+number+1.000.000+C%23&source=bl&ots=My-lTWC11T&sig=72yBtJ9sPi4YswtvWZgff-WFNco&hl=en&sa=X&redir_esc=y#v=onepage&q=1.000.000&f=false
            CultureInfo cul = new CultureInfo("zh-Hant");
            float fCoin = float.Parse(coin);
            coin = fCoin.ToString("N", cul);
            coin = coin.Substring(0, coin.LastIndexOf("."));
            coin = coin.Replace(",", ".");
            return coin;
        }

        private void mListVietel_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Viettel item = (Viettel)((FrameworkElement)sender).DataContext;
            String id = item.mobile_card_id;
            callApiDoiThuong(id);
            mMainPage.sendDeviceInfo("Đổi thưởng", item.mobile_card_type_name+" "+item.mobile_card_price);
        }

        private void mListVina_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Vinaphone item = (Vinaphone)((FrameworkElement)sender).DataContext;
            String id = item.mobile_card_id;
            callApiDoiThuong(id);
            mMainPage.sendDeviceInfo("Đổi thưởng", item.mobile_card_type_name + " " + item.mobile_card_price);
        }

        private void mListMobi_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MobiFone item = (MobiFone)((FrameworkElement)sender).DataContext;
            String id = item.mobile_card_id;
            callApiDoiThuong(id);
            mMainPage.sendDeviceInfo("Đổi thưởng", item.mobile_card_type_name + " " + item.mobile_card_price);
        }
        private void callApiDoiThuong(string id)
        {           
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add(ApiKey.MOBILE_CARD_ID, id);
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            Progressbar.Visibility = Visibility.Visible;
            BaseRequest.get(ApiUrl.URL_GET_CARD, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                Dispatcher.BeginInvoke(() =>
                {
                    JObject o = JObject.Parse(data);
                    string msg = (string)o.SelectToken("message");
                    String coin = (string)o.SelectToken("coin");
                    mMainPage.setToolbarMoney(coin);
                    mCoin.Text = string.Format("Số tiền có thể rút: {0} C (1C~1VNĐ)", convertCoin(coin));
                    MessageBox.Show(msg);
                    Progressbar.Visibility = Visibility.Collapsed;
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {                  
                    Progressbar.Visibility = Visibility.Collapsed;
                    BaseRequest.showErrorBox(reason);
                });
            });
        }
    }

    public class Viettel
    {
        public string mobile_card_id { get; set; }
        public string mobile_card_price { get; set; }
        public string mobile_card_status { get; set; }
        public string mobile_card_type_name { get; set; }
        public string mobile_card_img { get; set; }
    }

    public class Vinaphone
    {
        public string mobile_card_id { get; set; }
        public string mobile_card_price { get; set; }
        public string mobile_card_status { get; set; }
        public string mobile_card_type_name { get; set; }
        public string mobile_card_img { get; set; }
    }

    public class MobiFone
    {
        public string mobile_card_id { get; set; }
        public string mobile_card_price { get; set; }
        public string mobile_card_status { get; set; }
        public string mobile_card_type_name { get; set; }
        public string mobile_card_img { get; set; }
    }

    public class ArrCards
    {
        public List<Viettel> Viettel { get; set; }
        public List<Vinaphone> Vinaphone { get; set; }
        public List<MobiFone> MobiFone { get; set; }
    }

    public class ObjDoiThuong
    {
        public int returnVal { get; set; }
        public string coin { get; set; }
        public ArrCards arr_cards { get; set; }
    }
}
