﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PivotContentDemo.Logins;
using PivotContentDemo.Request;
using System.IO;
using PivotContentDemo.SubScreen;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Collections.ObjectModel;

namespace PivotContentDemo
{
    public partial class ThongBao : UserControl
    {
        private ObjLogin mObjLogin;
        private MainPage mMainPage;
        private ObservableCollection<ArrNotification> mArrData
        {
            get;
            set;
        }

        public ThongBao(MainPage mainPage)
        {
            mMainPage = mainPage;
            this.InitializeComponent();
            mArrData = new ObservableCollection<ArrNotification>();
            mObjLogin = SharePreference.getUserInfo();
            loadData(false);
        }

        private void PullupLoadmore_RefreshRequested(object sender, EventArgs e)
        {
            loadData(true);
        }

        private void refreshPanelRefreshRequested(object sender, EventArgs e)
        {
            loadData(false);
        }

        private void RefreshTab(object sender, System.Windows.Input.GestureEventArgs e)
        {
            loadData(false);
        }

        private void loadData(bool isLoadmore)
        {
            Progressbar.Visibility = Visibility;
            if (!isLoadmore)
            {
                mArrData.Clear();
            }
            Dictionary<String, String> param = new Dictionary<string, string>();
            param.Add(ApiKey.OFFSET, "" + mArrData.Count);
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            BaseRequest.get(ApiUrl.URL_THONGBAO, param, null, (stream) =>
            {
               
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                ObjThongBao rootObject = new ObjThongBao();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(rootObject.GetType());
                rootObject = ser.ReadObject(ms) as ObjThongBao;
                Dispatcher.BeginInvoke(() =>
                {
                    mMainPage.setNotification("0");
                    List<ArrNotification> arrData = rootObject.arr_notification;
                    for (int i = 0; i < arrData.Count; i++)
                    {
                        ArrNotification app = arrData[i];
                        mArrData.Add(arrData[i]);
                    }
                    list.ItemsSource = mArrData;
                    updateUI(mArrData);
                    Progressbar.Visibility = Visibility.Collapsed;
                    
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    updateUI(null);                   
                    BaseRequest.showErrorBox(mMainPage, reason);
                });
            });
        }
        

        private void updateUI(ObservableCollection<ArrNotification> arrApp)
        {
             if (arrApp == null)
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không tải được";
            } else if (arrApp.Count > 0)
            {
                ContentView.Visibility = Visibility.Visible;
                ErrorView.Visibility = Visibility.Collapsed;
            }
            else
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không có dữ liệu";
            }
        }

        private void ItemClickListener(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ContentView.IsHitTestVisible = false;
            ArrNotification item = (ArrNotification)((FrameworkElement)sender).DataContext;
            mDialogTitle.Text = item.title;
            mDialogWebView.NavigateToString(item.content);
            mThongBaoDialog.Visibility = Visibility.Visible;
        }

        private void mDialogBackBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ContentView.IsHitTestVisible = true;
            mThongBaoDialog.Visibility = Visibility.Collapsed;
        }
    }

    public class ArrNotification
    {
        public string content { get; set; }
        public string title { get; set; }
        public string date { get; set; }
    }

    public class ObjThongBao
    {
        public List<ArrNotification> arr_notification { get; set; }
    }
}
