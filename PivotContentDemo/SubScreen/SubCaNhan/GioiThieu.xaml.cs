﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using PivotContentDemo.Logins;
using Newtonsoft.Json;

namespace PivotContentDemo.SubScreen.SubCaNhan
{
    public partial class GioiThieu : PhoneApplicationPage
    {
        public GioiThieu()
        {
            InitializeComponent();
            ObjLogin  mObjLogin = SharePreference.getUserInfo();
            setToolbarMoney(mObjLogin.data.coin);
        }

        public void setToolbarMoney(string coin)
        {
            ObjLogin mObjLogin = SharePreference.getUserInfo();
            try
            {
                if (mObjLogin != null)
                    mObjLogin.data.coin = coin;
                var json = JsonConvert.SerializeObject(mObjLogin);
                SharePreference.saveUserInfo(json);
            }
            catch (Exception e)
            {

            }
            ToolbarMoney.Text = MainPage.convertCoin(coin);

        }

        private void BackBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }

        }

        private void Guimail_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var task = new EmailComposeTask { To = "contact@tui3gang.vn" };
            task.Show();
        }

        private void TrangWeb_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WebBrowserTask task = new WebBrowserTask();
            task.Uri =new Uri("http://www.tui3gang.vn");
            task.Show();
        }
        private void Fanpage_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string url = "https://www.facebook.com/T%C3%BAi-Ba-Gang-Ki%E1%BA%BFm-ti%E1%BB%81n-Online-1038850976198188/?fref=ts";
            WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri(url);
            task.Show();
        }
        private async void LienHe_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("skype:khanhtd91?chat");
            await Windows.System.Launcher.LaunchUriAsync(uri);

        }
        private void GoiDien_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();
            phoneCallTask.PhoneNumber = "01689999999";
           phoneCallTask.Show();
        }        
    }
}