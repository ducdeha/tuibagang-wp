﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PivotContentDemo.Logins;
using PivotContentDemo.Request;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using PivotContentDemo.SubScreen.SubCaNhan;
using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace PivotContentDemo.SubScreen.SubCaNhan
{
    public partial class LsHoatDong : PhoneApplicationPage
    {
        private ObjLogin mObjLogin;
        private ObservableCollection<ArrActivity> mArrData
        {
            get;
            set;
        }

        public LsHoatDong()
        {

            this.InitializeComponent();
            mArrData = new ObservableCollection<ArrActivity>();
            mObjLogin = SharePreference.getUserInfo();
            setToolbarMoney(mObjLogin.data.coin);
            loadData(false);
        }

        private void PullupLoadmore_RefreshRequested(object sender, EventArgs e)
        {
            loadData(true);
        }

        private void refreshPanelRefreshRequested(object sender, EventArgs e)
        {
            loadData(false);
        }

        private void RefreshTab(object sender, System.Windows.Input.GestureEventArgs e)
        {
            loadData(false);
        }

        private void loadData(bool isLoadmore)
        {
            Progressbar.Visibility = Visibility;
            if (!isLoadmore)
            {
                mArrData.Clear();
            }
            Dictionary<String, String> param = new Dictionary<string, string>();
            param.Add(ApiKey.OFFSET, "" + mArrData.Count);
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            BaseRequest.get(ApiUrl.URL_ACTIVITY, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                ObjLsHoatDong rootObject = new ObjLsHoatDong();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(rootObject.GetType());
                rootObject = ser.ReadObject(ms) as ObjLsHoatDong;
                Dispatcher.BeginInvoke(() =>
                {
                    List<ArrActivity> arrData = rootObject.arr_activitys;
                    for (int i = 0; i < arrData.Count; i++)
                    {
                        ArrActivity app = arrData[i];
                        app.strCoin = DoiThuong.convertCoin(app.coin+"");
                        mArrData.Add(app);
                    }
                    list.ItemsSource = mArrData;
                    updateUI(mArrData);
                    Progressbar.Visibility = Visibility.Collapsed;
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    updateUI(null);
                    Progressbar.Visibility = Visibility.Collapsed;
                    BaseRequest.showErrorBox(reason);
                });
            });
        }


        private void updateUI(ObservableCollection<ArrActivity> arrApp)
        {
             if (arrApp == null)
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không tải được";
            }else if (arrApp.Count > 0)
            {
                ContentView.Visibility = Visibility.Visible;
                ErrorView.Visibility = Visibility.Collapsed;
            }
            else
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không có dữ liệu";
            }
        }

        private void BackBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }

        }

        public void setToolbarMoney(string coin)
        {
            ObjLogin mObjLogin = SharePreference.getUserInfo();
            try
            {
                if (mObjLogin != null)
                    mObjLogin.data.coin = coin;
                var json = JsonConvert.SerializeObject(mObjLogin);
                SharePreference.saveUserInfo(json);
            }
            catch (Exception e)
            {

            }
            ToolbarMoney.Text = MainPage.convertCoin(coin);

        }
    }

    public class ArrActivity
    {
        public string activity_id { get; set; }
        public string date { get; set; }
        public string content { get; set; }
        public int coin { get; set; }
        public string strCoin { get; set; }
    }

    public class ObjLsHoatDong
    {
        public List<ArrActivity> arr_activitys { get; set; }
    }
    
}
