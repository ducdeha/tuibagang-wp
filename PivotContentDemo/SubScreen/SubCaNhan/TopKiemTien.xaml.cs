﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PivotContentDemo.Logins;
using PivotContentDemo.Request;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace PivotContentDemo.SubScreen.SubCaNhan
{
    public partial class TopKiemTien : PhoneApplicationPage
    {

        private ObjLogin mObjLogin;
        private bool misFromMain;
        private ObservableCollection<ArrRank> mArrData
        {
            get;
            set;
        }

        public TopKiemTien()
        {
            init();
        }

        public TopKiemTien(bool isFromMain)
        {
            init();
            if (isFromMain) Toolbar.Visibility = Visibility.Collapsed;
        }

        private void init()
        {
            this.InitializeComponent();
            mArrData = new ObservableCollection<ArrRank>();
            mObjLogin = SharePreference.getUserInfo();
            setToolbarMoney(mObjLogin.data.coin);
            loadData(false);
        }

        private void PullupLoadmore_RefreshRequested(object sender, EventArgs e)
        {
            loadData(true);
        }

        private void refreshPanelRefreshRequested(object sender, EventArgs e)
        {
            loadData(false);
        }

        private void RefreshTab(object sender, System.Windows.Input.GestureEventArgs e)
        {
            loadData(false);
        }

        private void loadData(bool isLoadmore)
        {
            Progressbar.Visibility = Visibility;
            if (!isLoadmore)
            {
                mArrData.Clear();
            }
            Dictionary<String, String> param = new Dictionary<string, string>();
            param.Add(ApiKey.OFFSET, "" + mArrData.Count);
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            BaseRequest.get(ApiUrl.URL_TOP_RANK, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                ObjTopRank rootObject = new ObjTopRank();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(rootObject.GetType());
                rootObject = ser.ReadObject(ms) as ObjTopRank;
                Dispatcher.BeginInvoke(() =>
                {
                    List<ArrRank> arrData = rootObject.arr_rank;
                    for (int i = 0; i < arrData.Count; i++)
                    {
                        ArrRank app = arrData[i];
                        app.coin = DoiThuong.convertCoin(app.coin + "");
                        app.position = mArrData.Count + 1;
                        setupListItem(app);
                        mArrData.Add(app);
                    }
                    list.ItemsSource = mArrData;
                    updateUI(mArrData);
                    Progressbar.Visibility = Visibility.Collapsed;
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    updateUI(null);
                    Progressbar.Visibility = Visibility.Collapsed;
                    BaseRequest.showErrorBox(reason);
                });
            });
        }


        private void updateUI(ObservableCollection<ArrRank> arrApp)
        {
            if (arrApp.Count > 0)
            {
                ContentView.Visibility = Visibility.Visible;
                ErrorView.Visibility = Visibility.Collapsed;
            }
            else if (arrApp == null)
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không tải được";
            }
            else
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không có dữ liệu";
            }
        }

        private void BackBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }

        }

        public void setToolbarMoney(string coin)
        {
            ObjLogin mObjLogin = SharePreference.getUserInfo();
            try
            {
                if (mObjLogin != null)
                    mObjLogin.data.coin = coin;
                var json = JsonConvert.SerializeObject(mObjLogin);
                SharePreference.saveUserInfo(json);
            }
            catch (Exception e)
            {

            }
            ToolbarMoney.Text = MainPage.convertCoin(coin);

        }

        private void setupListItem(ArrRank app)
        {
            if (app.position > 3)
            {
                app.width = 30;
                app.borderColor = "#aaa";
            }
            else
            {
                app.borderColor = "#f7a61b";
                app.width = 35;
            }
        }
    }

    public class ArrRank
    {
        public string publisher_facebook_name { get; set; }
        public string coin { get; set; }
        public int position { get; set; }
        public string borderColor { get; set; }
        public int width { get; set; }
    }

    public class ObjTopRank
    {
        public List<ArrRank> arr_rank { get; set; }
    }
}