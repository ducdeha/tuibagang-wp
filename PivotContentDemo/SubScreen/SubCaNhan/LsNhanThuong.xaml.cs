﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PivotContentDemo.Logins;
using Newtonsoft.Json;
using PivotContentDemo.Request;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;

namespace PivotContentDemo.SubScreen.SubCaNhan
{
    public partial class LsNhanThuong : PhoneApplicationPage
    {
        private ObjLogin mObjLogin;
        private ObservableCollection<ArrHistory> mArrData
        {
            get;
            set;
        }

        public LsNhanThuong()
        {

            this.InitializeComponent();
            mArrData = new ObservableCollection<ArrHistory>();
            mObjLogin = SharePreference.getUserInfo();
            setToolbarMoney(mObjLogin.data.coin);
            loadData(false);
        }

        private void PullupLoadmore_RefreshRequested(object sender, EventArgs e)
        {
            loadData(true);
        }

        private void refreshPanelRefreshRequested(object sender, EventArgs e)
        {
            loadData(false);
        }

        private void RefreshTab(object sender, System.Windows.Input.GestureEventArgs e)
        {
            loadData(false);
        }

        private void loadData(bool isLoadmore)
        {
            Progressbar.Visibility = Visibility;
            if (!isLoadmore)
            {
                mArrData.Clear();
            }
            Dictionary<String, String> param = new Dictionary<string, string>();
            param.Add(ApiKey.OFFSET, "" + mArrData.Count);
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            BaseRequest.get(ApiUrl.URL_GET_CARD_HISTORY, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                ObjDoiThuong rootObject = new ObjDoiThuong();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(rootObject.GetType());
                rootObject = ser.ReadObject(ms) as ObjDoiThuong;
                Dispatcher.BeginInvoke(() =>
                {
                    List<ArrHistory> arrData = rootObject.arr_history;
                    for (int i = 0; i < arrData.Count; i++)
                    {
                        ArrHistory app = arrData[i];
                        setItemState(app,true);
                        app.position = mArrData.Count;
                        mArrData.Add(app);
                    }
                   
                    list.ItemsSource = mArrData;
                    updateUI(mArrData);
                    Progressbar.Visibility = Visibility.Collapsed;
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    updateUI(null);
                    Progressbar.Visibility = Visibility.Collapsed;
                    BaseRequest.showErrorBox(reason);
                });
            });
        }

        private void setItemState(ArrHistory item,bool isChangeCardName)
        {
            int status = item.status;
            if(isChangeCardName)
            item.mobile_card_type_name = item.mobile_card_type_name + " " + DoiThuong.convertCoin(item.price) + " VNĐ";
            switch (status)
            {
                case 0:
                    //Cho duyet
                    item.strStatus = "Chờ duyệt";
                    item.statusColor = "#DE4847";
                    item.lineColor = "#DE4847";
                    item.tenTheColor = "#000000";
                    item.huyState = "Visible";//Collapsed
                    item.strHuy = "Hủy";
                    item.huyColor = "#f7a61b";
                    item.nhanState = "Collapsed";
                    break;
                case 1:
                    //Doi the thanh cong
                    item.strStatus = "Đổi thành công";
                    item.statusColor = "#1FB701";
                    item.lineColor = "#1FB701";
                    item.tenTheColor = "#000000";
                    item.huyState = "Collapsed";//Collapsed
                    item.nhanState = "Visible";
                    item.card.number_code = "Mã số thẻ: " + item.card.number_code;
                    item.card.seri = "Seri thẻ: " + item.card.seri;
                    break;
                case 2:
                    item.statusVisibility = "Collapsed";
                    item.statusColor = "#A6A5A6";
                    item.lineColor = "#A6A5A6";
                    item.tenTheColor = "#A6A5A6";
                    item.huyState = "Visible";//Collapsed
                    item.strHuy = "Đã hủy";
                    item.huyColor = "#A6A5A6";
                    item.nhanState = "Collapsed";
                    break;
                default: break;
            }
        }

        private void updateUI(ObservableCollection<ArrHistory> arrApp)
        {
            if (arrApp == null)
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không tải được";
            }
            else if (arrApp.Count > 0)
            {
                ContentView.Visibility = Visibility.Visible;
                ErrorView.Visibility = Visibility.Collapsed;
            }
            else

            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không có dữ liệu";
            }
        }

        private void BackBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));

        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            base.OnBackKeyPress(e);
        }

        public void setToolbarMoney(string coin)
        {
            ObjLogin mObjLogin = SharePreference.getUserInfo();
            try
            {
                if (mObjLogin != null)
                    mObjLogin.data.coin = coin;
                var json = JsonConvert.SerializeObject(mObjLogin);
                SharePreference.saveUserInfo(json);
            }
            catch (Exception e)
            {

            }
            ToolbarMoney.Text = MainPage.convertCoin(coin);

        }

        private void HuyBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ArrHistory item =(ArrHistory) ((FrameworkElement)sender).DataContext;
            if (!item.strHuy.Equals("Hủy")) return;
            Dictionary<String, String> param = new Dictionary<String, String>();
            param.Add(ApiKey.MOBILE_CARD_HISTORY_ID, item.mobile_card_history_id);
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            Progressbar.Visibility = Visibility.Visible;
            BaseRequest.post(ApiUrl.URL_CANCEL_REQUEST, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                Dispatcher.BeginInvoke(() =>
                {
                    JObject jObject = JObject.Parse(data);
                    string coin = (string)jObject["coin"];
                    setToolbarMoney(coin);
                    item.status = 2;
                    setItemState(item,false);
                    mArrData[item.position] = item;
                    list.ItemsSource = mArrData;
                    updateUI(mArrData);
                    Progressbar.Visibility = Visibility.Collapsed;
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    updateUI(null);
                    Progressbar.Visibility = Visibility.Collapsed;
                    BaseRequest.showErrorBox(reason);
                });
            });

        }
    }

    public class Card
    {
        public string date_time { get; set; }
        public string number_code { get; set; }
        public string seri { get; set; }
    }

    public class ArrHistory
    {
        public string mobile_card_history_id { get ; set; }
        public string date_time { get; set; }
        public string mobile_card_type_name { get; set; }
        public string price { get; set; }
        public int status { get; set; }
        public Card card { get; set; }
        public string lineColor { get; set; }
        public string strStatus { get; set; }
        public string huyState { get; set; }
        public string nhanState { get; set; }
        public string statusColor { get; set; }
        public string tenTheColor { get; set; }
        public string strHuy { get; set; }
        public string huyColor { get; set; }
        public string statusVisibility { get; set; }
        public int position { get; set; }
    }

    public class ObjDoiThuong
    {
        public List<ArrHistory> arr_history { get; set; }
    }
}
