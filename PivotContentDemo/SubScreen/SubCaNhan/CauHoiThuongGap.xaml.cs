﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PivotContentDemo.Logins;
using PivotContentDemo.Request;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace PivotContentDemo.SubScreen.SubCaNhan
{
    public partial class CauHoiThuongGap : PhoneApplicationPage
    {
        private ObjLogin mObjLogin;
        private ObservableCollection<ArrFaq> mArrData
        {
            get;
            set;
        }

        public CauHoiThuongGap()
        {

            this.InitializeComponent();
            mArrData = new ObservableCollection<ArrFaq>();
            mObjLogin = SharePreference.getUserInfo();
            setToolbarMoney(mObjLogin.data.coin);
            loadData(false);
        }

        private void PullupLoadmore_RefreshRequested(object sender, EventArgs e)
        {
            loadData(true);
        }

        private void refreshPanelRefreshRequested(object sender, EventArgs e)
        {
            loadData(false);
        }

        private void RefreshTab(object sender, System.Windows.Input.GestureEventArgs e)
        {
            loadData(false);
        }

        private void loadData(bool isLoadmore)
        {
            Progressbar.Visibility = Visibility;
            if (!isLoadmore)
            {
                mArrData.Clear();
            }
            Dictionary<String, String> param = new Dictionary<string, string>();
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            BaseRequest.get(ApiUrl.URL_CAU_HOI, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                ObjCauHoi rootObject = new ObjCauHoi();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(rootObject.GetType());
                rootObject = ser.ReadObject(ms) as ObjCauHoi;
                Dispatcher.BeginInvoke(() =>
                {
                    List<ArrFaq> arrData = rootObject.arr_faq;
                    for (int i = 0; i < arrData.Count; i++)
                    {
                        ArrFaq app = arrData[i];
                        app.admin_faq_question = (mArrData.Count + 1) +". "+ app.admin_faq_question;
                        mArrData.Add(app);
                    }
                    list.ItemsSource = mArrData;
                    updateUI(mArrData);
                    Progressbar.Visibility = Visibility.Collapsed;
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    updateUI(null);                   
                    BaseRequest.showErrorBox(reason);
                });
            });
        }


        private void updateUI(ObservableCollection<ArrFaq> arrApp)
        {
           if (arrApp == null)
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không tải được";
            }
            else if (arrApp.Count > 0)
            {
                ContentView.Visibility = Visibility.Visible;
                ErrorView.Visibility = Visibility.Collapsed;
            }
            else
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không có dữ liệu";
            }
        }

        private void BackBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }

        }

        public void setToolbarMoney(string coin)
        {
            ObjLogin mObjLogin = SharePreference.getUserInfo();
            try
            {
                if (mObjLogin != null)
                    mObjLogin.data.coin = coin;
                var json = JsonConvert.SerializeObject(mObjLogin);
                SharePreference.saveUserInfo(json);
            }
            catch (Exception e)
            {

            }
            ToolbarMoney.Text = MainPage.convertCoin(coin);
        }
    }

    public class ArrFaq
    {
        public string admin_faq_question { get; set; }
        public string admin_faq_answer { get; set; }
    }

    public class ObjCauHoi
    {
        public List<ArrFaq> arr_faq { get; set; }
    }
}