﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PivotContentDemo.Logins;
using System.Collections.ObjectModel;
using PivotContentDemo.Request;
using Newtonsoft.Json;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows.Media;
using System.Globalization;

namespace PivotContentDemo.SubScreen.SubCaNhan
{
    public partial class LienHe : PhoneApplicationPage
    {

        private ObjLogin mObjLogin;
        private ObservableCollection<ArrContact> mArrData
        {
            get;
            set;
        }

        private List<ArrContact> mListData
        {
            get;
            set;
        }

        public LienHe()
        {

            this.InitializeComponent();
            mArrData = new ObservableCollection<ArrContact>();
            mListData = new List<ArrContact>();
            mObjLogin = SharePreference.getUserInfo();
            setToolbarMoney(mObjLogin.data.coin);
            loadData(false);

        }

        private void PullupLoadmore_RefreshRequested(object sender, EventArgs e)
        {
            loadData(false);
        }

        private void refreshPanelRefreshRequested(object sender, EventArgs e)
        {
            loadData(true);
        }

        private void RefreshTab(object sender, System.Windows.Input.GestureEventArgs e)
        {
            loadData(false);
        }

        private void loadData(bool isLoadmore)
        {
            Progressbar.Visibility = Visibility;
            if (!isLoadmore)
            {
                mListData.Clear();
                mArrData.Clear();
            }
            Dictionary<String, String> param = new Dictionary<string, string>();
            param.Add(ApiKey.OFFSET, "" + mArrData.Count);
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            BaseRequest.get(ApiUrl.URL_LIST_CONTACT, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                ObjLienHe rootObject = new ObjLienHe();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(rootObject.GetType());
                rootObject = ser.ReadObject(ms) as ObjLienHe;
                Dispatcher.BeginInvoke(() =>
                {
                    List<ArrContact> arrData = rootObject.arr_contact;
                    for (int i = 0; i < arrData.Count; i++)
                    {
                        ArrContact app = arrData[i];
                        if (app.status == 1)
                        {
                            app.meVisible = "Visible";
                            app.adminVisible = "Collapsed";
                        }
                        else
                        {
                            app.adminVisible = "Visible";
                            app.meVisible = "Collapsed";
                        }
                        app.myAvatar = mObjLogin.data.publisher_avatar_url;
                        app.timeInMili = ArrContact.convertDateToMili(app.time);                  
                        mListData.Add(app);
                    }
                    mListData.Sort();
                    mArrData = new ObservableCollection<ArrContact>(mListData);
                    list.ItemsSource = mArrData;
                    updateUI(mArrData);
                    Progressbar.Visibility = Visibility.Collapsed;
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    updateUI(null);
                    Progressbar.Visibility = Visibility.Collapsed;
                    BaseRequest.showErrorBox(reason);
                });
            });
        }


        private void updateUI(ObservableCollection<ArrContact> arrApp)
        {
            if (arrApp == null)
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không tải được";
            }
            else if (arrApp.Count > 0)
            {
                ContentView.Visibility = Visibility.Visible;
                ErrorView.Visibility = Visibility.Collapsed;
            }
            else
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không có dữ liệu";
            }
        }

        private void BackBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));

        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            base.OnBackKeyPress(e);
        }

        public void setToolbarMoney(string coin)
        {
            ObjLogin mObjLogin = SharePreference.getUserInfo();
            try
            {
                if (mObjLogin != null)
                    mObjLogin.data.coin = coin;
                var json = JsonConvert.SerializeObject(mObjLogin);
                SharePreference.saveUserInfo(json);
            }
            catch (Exception e)
            {

            }
            ToolbarMoney.Text = MainPage.convertCoin(coin);

        }

        private void Image_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string input = InputBox.Text;
            if (string.IsNullOrEmpty(input)) return;
            Dictionary<String, String> param = new Dictionary<string, string>();
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            param.Add(ApiKey.CONTENT, input);
            Progressbar.Visibility = Visibility.Visible;
            BaseRequest.post(ApiUrl.URL_CHATTING, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                ObjContact rootObject = new ObjContact();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(rootObject.GetType());
                rootObject = ser.ReadObject(ms) as ObjContact;
                ArrContact contact = rootObject.data;
                contact.myAvatar = mObjLogin.data.publisher_avatar_url;
                contact.meVisible = "Visible";
                contact.adminVisible = "Collapsed";
                Dispatcher.BeginInvoke(() =>
                {
                    mListData.Add(contact);
                    mArrData.Add(contact);
                    mArrData.OrderByDescending(a => a.time);
                    list.ItemsSource = mArrData;
                    updateUI(mArrData);
                    InputBox.Text = "";
                    Progressbar.Visibility = Visibility.Collapsed;
                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    updateUI(null);
                    Progressbar.Visibility = Visibility.Collapsed;
                    BaseRequest.showErrorBox(reason);
                });
            });
        }

    }

    public class ArrContact:IComparable<ArrContact> 
    {
        public string id { get; set; }
        public string content { get; set; }
        public int status { get; set; }
        public string time { get; set; }
        public string myAvatar { get; set; }
        public string adminVisible { get; set; }
        public string meVisible { get; set; }
        public long timeInMili { get; set; }
    

        public static long convertDateToMili(String date)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            DateTime dateTime = DateTime.ParseExact(date, "dd/MM/yyyy HH:mm:ss", provider);
            long dm = (long)(dateTime - new DateTime(1970, 1, 1)).TotalMilliseconds;
            return dm;
        }

        public int CompareTo(ArrContact other)
        {
            if (this.timeInMili > other.timeInMili) return 1; else return -1;
        }
    }

    public class ObjLienHe
    {
        public List<ArrContact> arr_contact { get; set; }
    }

    public class ObjContact
    {
        public ArrContact data { get; set; }
    }

}
