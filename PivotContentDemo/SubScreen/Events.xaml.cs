﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PivotContentDemo.Request;
using System.Threading.Tasks;
using System.Text;
using PivotContentDemo.Logins;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;
using Microsoft.Phone.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;

namespace PivotContentDemo.SubScreen
{
    public partial class Events : UserControl
    {

        private ObjLogin mObjLogin;
        private ArrEvent mItemClicked;
        private MainPage mMainPage;
        private ObservableCollection<ArrEvent> mArrData
        {
            get;
            set;
        }

        public Events(MainPage mainPage)
        {
            this.InitializeComponent();
            ShareSourceLoad();
            mMainPage = mainPage;
            mArrData = new ObservableCollection<ArrEvent>();
            mObjLogin = SharePreference.getUserInfo();
            loadData(false);
        }

        private void PullupLoadmore_RefreshRequested(object sender, EventArgs e)
        {
            loadData(true);
        }

        private void refreshPanelRefreshRequested(object sender, EventArgs e)
        {
            loadData(false);
        }

        private void RefreshTab(object sender, System.Windows.Input.GestureEventArgs e)
        {
            loadData(false);
        }

        private void loadData(bool isLoadmore)
        {
            Progressbar.Visibility = Visibility;
            if (!isLoadmore)
            {
                mArrData.Clear();
            }
            Dictionary<String, String> param = new Dictionary<string, string>();
            param.Add(ApiKey.OFFSET, "" + mArrData.Count);
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);

            BaseRequest.get(ApiUrl.URL_GET_LIST_EVENTS, param, null, (stream) =>
            {
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                ObjEvent objEvent = new ObjEvent();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(objEvent.GetType());
                objEvent = ser.ReadObject(ms) as ObjEvent;
                Dispatcher.BeginInvoke(() =>
                {
                    List<ArrEvent> arrData = objEvent.arr_events;
                    for (int i = 0; i < arrData.Count; i++)
                    {
                        ArrEvent app = arrData[i];
                        mArrData.Add(arrData[i]);
                    }
                    list.ItemsSource = mArrData;
                    updateUI(mArrData);
                    Progressbar.Visibility = Visibility.Collapsed;

                });
            }, (reason) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    updateUI(null);
                    BaseRequest.showErrorBox(mMainPage, reason);
                });
            });
        }

        private void updateUI(ObservableCollection<ArrEvent> arrApp)
        {
            if (arrApp == null)
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không tải được";
            }
            else if (arrApp.Count > 0)
            {
                ContentView.Visibility = Visibility.Visible;
                ErrorView.Visibility = Visibility.Collapsed;
            }

            else
            {
                ContentView.Visibility = Visibility.Collapsed;
                ErrorView.Visibility = Visibility.Visible;
                ErrorViewText.Text = "Không có dữ liệu";
            }
        }

        private void mDialogOkBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ContentView.IsHitTestVisible = true;
            mDialog.Visibility = Visibility.Collapsed;
            int eventType = int.Parse(mItemClicked.event_type);
            mMainPage.sendDeviceInfo("Tham gia Event","Tham gia event: "+mItemClicked.event_info);
            if (eventType == 2)
            {
                //Chia se
                callApiShare(mItemClicked.event_id);
            } else if ( eventType == 3)
            {
                //Click to web
                String url = mItemClicked.event_link_share;
                if (!url.StartsWith("http")) url = "http://" + url;
                WebBrowserTask task = new WebBrowserTask();
                task.Uri = new Uri(url);
                task.Show();
            }
            else
            {
                //Nhap lieu
                callApiFeedback();
            }
        }

        private void callApiFeedback()
        {
            string input = mDialogInput.Text;
            if (string.IsNullOrEmpty(input)) return;
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            param.Add(ApiKey.EVENT_ID, mItemClicked.event_id);
            param.Add(ApiKey.CONTENT, input);
            Progressbar.Visibility = Visibility.Visible;
            BaseRequest.get(ApiUrl.URL_EVENT_FEEDBACK, param, null, (result) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    MessageBox.Show("Bạn đã tham gia sự kiện thành công. Vui lòng chờ admin duyệt thưởng trong thời gian sớm nhất.", "Thành công", new MessageBoxButton());
                    loadData(false);
                });
            }, (error) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                });
            });
        }

        private void callApiShare(string id)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add(ApiKey.PUBLISHER_USER_ID, mObjLogin.data.publisher_user_id);
            param.Add(ApiKey.EVENT_ID, id);
            Progressbar.Visibility = Visibility.Visible;
            BaseRequest.get(ApiUrl.URL_SHARE_EVENT, param, null, (stream) =>
            {              
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                    JObject o = JObject.Parse(data);
                    string coin = (string)o.SelectToken("coin");
                    mMainPage.setToolbarMoney(coin);                  
                    DataTransferManager.ShowShareUI();
                });
            }, (error) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Progressbar.Visibility = Visibility.Collapsed;
                });
            });
        }

        private void mDialogBackBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ContentView.IsHitTestVisible = true;
            mDialog.Visibility = Visibility.Collapsed;
        }

        private void ItemClickListener(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ContentView.IsHitTestVisible = false;
            mItemClicked = (ArrEvent)((FrameworkElement)sender).DataContext;
            mDialogTitle.Text = mItemClicked.event_title;
           // mDialogContent.Text = mItemClicked.event_info;
            int eventType = int.Parse(mItemClicked.event_type);
            if (eventType == 2)
            {
                mDialogOk.Text = "Chia sẻ";
                mDialogTextBox.Visibility = Visibility.Collapsed;
            }else if (eventType == 3)
            {                
                mDialogOk.Text = "Xem ngay";
                mDialogTextBox.Visibility = Visibility.Collapsed;
            }
            else
            {
                mDialogOk.Text = "Xong";
                mDialogTextBox.Visibility = Visibility.Visible;
            }
            mWebBrowser.NavigateToString(mItemClicked.event_info);
            mDialog.Visibility = Visibility.Visible;
        }

        private void ShareSourceLoad()
        {
            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
            dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.DataRequested);
        }

        private void DataRequested(DataTransferManager sender, DataRequestedEventArgs e)
        {
            DataRequest request = e.Request;
            request.Data.Properties.Title = "Chia sẻ: ";
            request.Data.Properties.Description = "";
            string shareContent = mItemClicked.event_info + ". Để biết thêm chi tiết, vui lòng truy cập: " + mItemClicked.event_link_share
                          + ".\nLưu ý: " + mItemClicked.event_note;
            request.Data.SetText(shareContent);
        }

    }

    public class ArrEvent
    {
        public string event_id { get; set; }
        public string event_title { get; set; }
        public string event_icon { get; set; }
        public object event_coin { get; set; }
        public string event_type { get; set; }
        public string event_day { get; set; }
        public string event_time { get; set; }
        public string event_info { get; set; }
        public string event_note { get; set; }
        public int event_status { get; set; }
        public string event_link_share { get; set; }
    }

    public class ObjEvent
    {
        public int returnVal { get; set; }
        public List<ArrEvent> arr_events { get; set; }
    }

}
