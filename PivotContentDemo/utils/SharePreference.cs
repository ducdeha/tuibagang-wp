﻿
namespace PivotContentDemo
{
    using System;
    using System.IO.IsolatedStorage;
    using Logins;
    using System.IO;
    using System.Runtime.Serialization.Json;
    using System.Text;
    public class SharePreference
    {
        public const string PREF_USER_INFO = "user_info";
        public const string PREF_GCM_DEVICE_TOKEN = "gcm_device_token";
        public const string PREF_APP_STATUS = "app_status";
        public const string PREF_FB_ACCESS_TOKEN = "facebook_access_token";
        public static void saveString(string key, string data)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            if (!settings.Contains(key))
            {
                settings.Add(key, data);
            }
            else
            {
                settings[key] = data;
            }
            settings.Save();
        }

        public static string getString(string key)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                return IsolatedStorageSettings.ApplicationSettings[key].ToString();             
            }
            else return "";
        }

        public static void removeString(string key)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                IsolatedStorageSettings.ApplicationSettings.Remove(key);
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }

        public static void saveUserInfo(String data)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            if (!settings.Contains(PREF_USER_INFO))
            {
                settings.Add(PREF_USER_INFO, data);
            }
            else
            {
                settings[PREF_USER_INFO] = data;
            }
            settings.Save();
        }

        public static ObjLogin getUserInfo()
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(PREF_USER_INFO))
            {
                string userInfo = IsolatedStorageSettings.ApplicationSettings[PREF_USER_INFO].ToString();
                ObjLogin mObjLogin = new ObjLogin();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(userInfo));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(mObjLogin.GetType());
                mObjLogin = ser.ReadObject(ms) as ObjLogin;
                return mObjLogin;
            }
            else return null;
        }

        public static void removeUserInfo()
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(PREF_USER_INFO))
            {
                IsolatedStorageSettings.ApplicationSettings.Remove(PREF_USER_INFO);
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }


    }
}
