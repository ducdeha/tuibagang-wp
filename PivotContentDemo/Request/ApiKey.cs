﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PivotContentDemo.Request
{
    class ApiKey
    {
        //Login
        public static String PUBLISHER_SOCIAL_ID = "publisher_social_id";
        public static String PUBLISHER_EMAIL = "publisher_email";
        public static String PUBLISHER_AVATAR_URL = "publisher_avatar_url";
        public static String PUBLISHER_FACEBOOK_NAME = "publisher_facebook_name";
        public static String PUBLISHER_CITY = "publisher_city";
        public static String BIRTHDAY = "birthday";
        public static String PUBLISHER_SEX = "publisher_sex";
        public static String PUBLISHER_AGE = "publisher_age";
        public static String PUBLISHER_CAMPAIGN_ID = "campaign_id";
        public static String DEVICE_ID = "device_id";
        public static String GCM_ID = "gcm_id";
        public static String ACCEPT_TOKEN = "accept_token";
        public static String DETAIL = "message";
        public static String DEVICE_MODEL = "device_model";
        public static String DEVICE_MANUFACTORY = "device_factory";
        public static String DEVICE_VERSION = "device_version";
        public static String DEVICE_IP = "device_ip";
        public static String DEVICE_SERI = "device_seri";
        public static String DEVICE_MAC_ADDRESS = "device_mac_address";
        public static String EVENT_ID = "event_id";
        public static String CONTENT = "content";
        public static String SHARE_CODE = "share_code";

        public static String NETWORK_PROVIDER = "network_provider";
        public static String DEVICE_IMEI = "device_imei";
        public static String IS_WIFI_ENABLED = "is_wifi_enabled";
        public static String APP_VERSION = "app_version";
        public static String ACTION = "action";
        //Kiem tien
        public static String OFFSET = "offset";
        public static String PUBLISHER_USER_ID = "publisher_user_id";
        public static String MOBILE_CARD_ID = "mobile_card_id";
        public static String OS = "os";

        //Lich su doi thuong
        public static String MOBILE_CARD_HISTORY_ID = "mobile_card_history_id";
    }
}
