﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using PivotContentDemo.Logins;
using static PivotContentDemo.Login;
using Newtonsoft.Json.Linq;
using System.Windows;
using System.Windows.Navigation;

namespace PivotContentDemo.Request
{
    public class BaseRequest
    {
        public static void get(String url, Dictionary<String, String> getParams, Dictionary<String, String> extra_headers, RESTSuccessCallback success_callback, RESTErrorCallback error_callback)
        {                       
            if (getParams != null)
            {
                getParams.Add(ApiKey.OS, "3");
                addAcceptToken(getParams);
                StringBuilder stringBuilder = new StringBuilder();
                foreach (String param in getParams.Keys)
                {
                    stringBuilder.Append(param).Append("=").Append(getParams[param]).Append("&");
                }
                string newParams = stringBuilder.ToString();
                newParams = newParams.Substring(0, newParams.Length - 1);
                url = url + "?" + newParams;
            }
            Uri uri = new Uri(url);
            HttpWebRequest request = WebRequest.CreateHttp(uri);
            if (request.Headers == null)
            {
                request.Headers = new WebHeaderCollection();
            }
            request.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString();
            if (extra_headers != null)
                foreach (String header in extra_headers.Keys)
                    try
                    {
                        request.Headers[header] = extra_headers[header];
                    }
                    catch (Exception) { }

            request.BeginGetResponse((IAsyncResult result) =>
            {
                HttpWebRequest req = result.AsyncState as HttpWebRequest;
                if (req != null)
                {
                    try
                    {
                        WebResponse response = req.EndGetResponse(result);
                        if (success_callback != null)
                            success_callback(response.GetResponseStream());
                    }
                    catch (WebException e)
                    {
                        try { 
                        var resp = new StreamReader(e.Response.GetResponseStream()).ReadToEnd();
                        JObject o = JObject.Parse(resp);
                        string msg = (string)o.SelectToken("message");
                        if (string.IsNullOrEmpty(msg)) msg = "Đã có lỗi xảy ra";
                        if (error_callback != null)
                            error_callback(msg);
                        return;
                        }catch (Exception ee) { error_callback(""); }
                    }
                }
            }, request);
        }

        public static void post(String url, Dictionary<String, String> post_params, Dictionary<String, String> extra_headers, RESTSuccessCallback success_callback, RESTErrorCallback error_callback)
        {
            Uri uri = new Uri(url);
            addAcceptToken(post_params);
            HttpWebRequest request = WebRequest.CreateHttp(uri);
            request.ContentType = "application/json";
            request.Method = "POST";
            if (extra_headers != null)
                foreach (String header in extra_headers.Keys)
                    try
                    {
                        request.Headers[header] = extra_headers[header];
                    }
                    catch (Exception) { }
            request.BeginGetRequestStream((IAsyncResult result) =>
            {
                HttpWebRequest preq = result.AsyncState as HttpWebRequest;
                if (preq != null)
                {
                    Stream postStream = preq.EndGetRequestStream(result);
                    string paramsBuilder = MyDictionaryToJson(post_params);
                    Byte[] byteArray = Encoding.UTF8.GetBytes(paramsBuilder);
                    postStream.Write(byteArray, 0, byteArray.Length);
                    postStream.Close();
                    preq.BeginGetResponse((IAsyncResult final_result) =>
                    {
                        HttpWebRequest req = final_result.AsyncState as HttpWebRequest;
                        if (req != null)
                        {
                            try
                            {
                                WebResponse response = req.EndGetResponse(final_result);
                                success_callback(response.GetResponseStream());
                            }
                            catch (WebException e)
                            {
                                try
                                {
                                    var resp = new StreamReader(e.Response.GetResponseStream()).ReadToEnd();                             
                                    JObject o = JObject.Parse(resp);
                                    string msg = (string)o.SelectToken("message");
                                    if (string.IsNullOrEmpty(msg)) msg = "Đã có lỗi xảy ra";
                                    error_callback(msg);
                                    return;
                                }
                                catch (Exception error) { }
                            }
                        }
                    }, preq);
                }
            }, request);
        }
        private static string MyDictionaryToJson(Dictionary<String, String> dict)
        {
            var entries = dict.Select(d =>
                string.Format("\"{0}\": \"{1}\"", d.Key, string.Join(",", d.Value)));
            return "{" + string.Join(",", entries) + "}";
        }


        private static void addAcceptToken(Dictionary<String, String> param)
        {
            ObjLogin objLogin = SharePreference.getUserInfo();
            if (objLogin != null)
                param.Add("accept_token", objLogin.data.accept_token);
        }

        public static void showErrorBox(String msg)
        {         
                MessageBox.Show(msg, "Lỗi", MessageBoxButton.OK);
        }

        public static void showErrorBox(MainPage mainPage, string msg)
        {
            MessageBoxResult result = MessageBox.Show(msg, "Lỗi", MessageBoxButton.OK);
    
            if (result == MessageBoxResult.OK)
            {
                if (msg.Equals("Phiên làm việc đã hết hạn, bạn vui lòng đăng nhập lại"))
                {
                    SessionStorage.Remove();
                    SharePreference.removeUserInfo();
                    SharePreference.removeString(SharePreference.PREF_GCM_DEVICE_TOKEN);
                    mainPage.gotoNewPage("/Logins/Login.xaml");
                }
            }
        }

        internal static void get(string uRL_APP_STATUS, object p1, object p2, Action<Stream> p3, Action<string> p4)
        {
            throw new NotImplementedException();
        }
    }
}
