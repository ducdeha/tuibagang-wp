﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PivotContentDemo.Request
{
    public class ApiUrl
    {
       // public static String BASE_URL = "http://45.32.58.106:3001/";
        public static String BASE_URL = "http://tbg.thanbai69.com/";
        public static String URL_LOGIN = BASE_URL + "login";
        public static String URL_GET_LIST_APP = BASE_URL + "listapp";
        public static String URL_INSTALL_APP = BASE_URL + "installapp";
        public static String URL_GET_LIST_EVENTS = BASE_URL + "getListEvents";
        public static String URL_GET_LIST_CARD = BASE_URL + "getListCard";
        public static String URL_GET_CARD_HISTORY = BASE_URL + "getCardHistory";
        public static String URL_CANCEL_REQUEST = BASE_URL + "cancelRequest";
        public static String URL_ACTIVITY = BASE_URL + "activity";
        public static String URL_TOP_RANK = BASE_URL + "topRank";
        public static String URL_CAU_HOI = BASE_URL + "faq";
        public static String URL_THONGBAO = BASE_URL + "listNotification";
        public static String URL_GET_COIN = BASE_URL + "doquestion";
        public static String URL_EVENT_FEEDBACK = BASE_URL + "EventFeedback";
        public static String URL_GET_CARD = BASE_URL + "getCard";
        public static String URL_LIST_CONTACT = BASE_URL + "contact";
        public static String URL_CHATTING = BASE_URL + "chatting";
        public static String URL_CLICK_APP = BASE_URL + "clickapp";
        public static String URL_CHECK_INSTALL = BASE_URL + "checkinstall";
        public static String URL_SHARE_CODE = BASE_URL + "enterShareCode";
        public static String URL_SHARE_EVENT = BASE_URL + "sharing";
        public static String URL_BLOCK = BASE_URL + "block";
        public static String URL_LOG = BASE_URL + "access_log";
        public static String URL_APP_STATUS = BASE_URL + "windowsphone";
    }
}
